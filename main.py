from intra42API import intra42api
import json
from collections import Counter
from datetime import datetime


client_id = "e305f63c44ac0686622aece03a020202a2d42e0543f2f61d7ea794abe8b38c67"
client_secret = "7dabc6a2ad4433f601b27dc21271a1d8d0a4d939fb3c41d6bf888b35fc122d70"

i42api = intra42api.Intra42Api

token = i42api.token(client_id, client_secret)


user = {}


def where_minus_42(projects):
    for project in projects:
        if project["final_mark"] == -42:
            user['cheated_project'].append(project['project']['name'])


def get_last_log(login, count=90):
    locations = i42api.get_locations(token['access_token'], login, count)

    time = locations[0]['begin_at'].split('T')
    time = time[0] + " " + time[1].split('Z')[0]
    t = datetime.strptime(time, '%Y-%m-%d %H:%M:%S.%f')
    user["logs"]["last_logs"] = {
        "date": locations[0]["begin_at"],
        "host": locations[0]["host"],
        "passed_time": (datetime.now() - t).__str__().split('.')[0]
    }
    hosts = list(map(lambda el: el['host'], locations))
    c = Counter(hosts)
    most_common = c.most_common()[0][0]
    user["logs"]["favorite_place"] = {
        "host": most_common,
        "cluster": most_common.split('r')[0]
    }


def get_coalition_info(login):
    coalition = i42api.get_user_coalition(token['access_token'], login)
    info = i42api.get_user_info_in_coalition(token['access_token'], login)
    user['coalition'] = {
        'name': coalition[0]['name'],
        'rank': info[0]['rank']
    }


def get_corretions(login):
    r = i42api.get_first_correction(token['access_token'], login)
    user['corrections']['first_correction'] = r
    r = i42api.get_last_correction(token['access_token'], login)
    user['corrections']['last_correction'] = r
    r = i42api.get_last_correction(token['access_token'], login)
    user['corrections']['last_cheat_correction'] = r


def out_info(insurgent):
    user['login'] = insurgent['login']
    user['first_name'] = insurgent['first_name']
    user['last_name'] = insurgent['last_name']
    user['phone'] = insurgent['phone']
    user['email'] = insurgent['email']
    user['location'] = insurgent['location']
    user['wallet'] = insurgent['wallet']
    user['pool_year'] = insurgent['pool_year']
    user['correction_point'] = insurgent['correction_point']
    user['campus_name'] = insurgent['campus'][0]['name']
    user['cheated_project'] = []
    user['logs'] = {
        "last_logs": "non",
        "favorite_place": "non"
    }
    user['corrections'] = {
        'first_correction': 'non',
        'last_correction': 'non',
        'last_cheat_correction': 'non'
    }
    where_minus_42(insurgent['projects_users'])
    get_last_log(insurgent['login'])
    get_coalition_info(insurgent['login'])
    get_corretions(insurgent['login'])
    print(json.dumps(user, indent=4, sort_keys=True))


while True:
    response = (input("Enter insurgent name or exit: ").rstrip(' \t\n\r'))
    if response == 'exit':
        break
    r = i42api.get_user_by_login(token['access_token'], response)
    if "login" not in r:
        print("Grate news, insurgent not found")
        continue
    out_info(r)


