import requester
import json
from .const import *


class Intra42Api:

    @staticmethod
    def token(client_id, client_secret):
        params = {
            "grant_type": "client_credentials",
            "client_id": client_id,
            "client_secret": client_secret
        }
        return json.loads(requester.Requester.post(INTRA_TOKEN, params=params).content)

    @staticmethod
    def get_coalitions(bearer):
        headers = {
            "Authorization": "Bearer " + bearer
        }
        return json.loads(requester.Requester.get(INTRA_COALITIONS, headers=headers).content)

    @staticmethod
    def get_user_by_login(token, id):
        headers = {
            "Authorization": "Bearer " + token
        }
        return json.loads(requester.Requester.get(INTRA_GET_USER_BY_ID + "/" + id, headers=headers).content)

    @staticmethod
    def get_locations(token, id, count):
        headers = {
            "Authorization": "Bearer " + token
        }
        params = {
            "page": 1,
            "per_page": count,
            "sort": "-begin_at"
        }
        return json.loads(
            requester.Requester.get(INTRA_GET_USER_BY_ID + "/" + id + "/locations", params=params, headers=headers).content)

    @staticmethod
    def get_user_coalition(token, id):
        headers = {
            "Authorization": "Bearer " + token
        }
        return json.loads(
            requester.Requester.get(INTRA_GET_USER_BY_ID + "/" + id + "/coalitions", headers=headers).content)

    @staticmethod
    def get_user_info_in_coalition(token, id):
        headers = {
            "Authorization": "Bearer " + token
        }
        return json.loads(
            requester.Requester.get(INTRA_GET_USER_BY_ID + "/" + id + "/coalitions_users", headers=headers).content)

    @staticmethod
    def get_first_correction(token, id):
        headers = {
            "Authorization": "Bearer " + token
        }
        params = {
            "sort": "begin_at",
            "per_page": 1
        }
        return json.loads(
            requester.Requester.get(INTRA_GET_USER_BY_ID + "/" + id + "/scale_teams/as_corrector", params=params, headers=headers).content)

    @staticmethod
    def get_last_correction(token, id):
        headers = {
            "Authorization": "Bearer " + token
        }
        params = {
            "sort": "-begin_at",
            "per_page": 1
        }
        return json.loads(
            requester.Requester.get(INTRA_GET_USER_BY_ID + "/" + id + "/scale_teams/as_corrector", params=params, headers=headers).content)

    @staticmethod
    def get_all_correction(token, id):
        headers = {
            "Authorization": "Bearer " + token
        }
        return json.loads(
            requester.Requester.get(INTRA_GET_USER_BY_ID + "/" + id + "/scale_teams/as_corrector", headers=headers).content)