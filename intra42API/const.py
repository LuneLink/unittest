INTRA_URL = "https://api.intra.42.fr"
INTRA_TOKEN = INTRA_URL + "/oauth/token"

INTRA_COALITIONS = INTRA_URL + "/v2/coalitions"
INTRA_GET_USER_BY_ID = INTRA_URL + "/v2/users"
