import requests


class Response:
    def __init__(self, content, code, headers):
        self.content = content
        self.code = code
        self.headers = headers


class Requester:

    @staticmethod
    def get(url, params=None, **kwargs):
        result = requests.get(url, params, **kwargs)
        return Response(result.content, result.status_code, result.headers)

    @staticmethod
    def post(url, data=None, json=None, **kwargs):
        result = requests.post(url, data, json, **kwargs)
        return Response(result.content, result.status_code, result.headers)
